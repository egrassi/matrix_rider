DEST?=$(BIOINFO_ROOT)/binary/$(BIOINFO_HOST)/local
LDFLAGS+=-lm -O2
OBJ = matrix_rider

.PHONY: all clean install

all: matrix_rider

clean:
	rm -f  $(OBJ)

install: $(OBJ)
	install -D -s $< $(DEST)/bin/$<

matrix_rider: fasta.c main.c matrixes.c CuTest.c input.c
	gcc $(CFLAGS) -o $@ $^ $(LDFLAGS)

matrix_rider_debug: fasta.c main.c matrixes.c CuTest.c input.c debug.c
	gcc -Wall -g -DDEBUG -o $@ $^ -lm

cuTest: all_tests.c fasta.c  CuTest.c input.c matrixes.c tests.c
	gcc  -g -o $@ $^ -lm

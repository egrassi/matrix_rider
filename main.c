/* main.c
//
// Copyright (C) 2008 Elena Grassi <grassi.e@gmail.com>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "total_affinity.h"

/*
    Function: main
    
    Drives the execution following command line arguments.
    
    Parameters:
    
    argc - int (default): the number of given arguments.
    argv - char** (default): array of arguments strings.  
*/
int main(int argc, char *argv[])
{

	run info = NULL;
	if (set_beginning_info(&info))
		return ERROR;

	parse_command_line(argv, argc, info);

#ifdef DEBUG
	printf("#Starting with window %d, revcomp %d, count %d, cutoff %lf, "
	       "abs_cutoff %lf, given_abs_cutoff %d, log2 %d, slide %d, only_max %d "
	       "error_status %d\n",
	       info->window, info->revcomp, info->counts, info->cutoff,
	       info->abs_cutoff, info->given_abs_cutoff, info->log2,
	       info->slide, info->only_max, info->error);
#endif				/* DEBUG */

	check_error(info);

	if (info->window == 0 && !info->miRNA && !info->occupancy)	/* if in syteseeker mode we always use loglikelihoods. */
		info->log2 = 1; 										/* but we don't use them in miRNA or occupancy mode */

	get_matrixes(info);
	check_error(info);

	if (info->f_cutoffs) {
		get_cutoffs(info);
		check_error(info);
	}
#ifdef DEBUG
	printf("#Loaded %d matrixes\n", info->n_matrixes);
	print_matrixes(info);
#endif				/* DEBUG */

	load_fasta(info);
	check_error(info);

#ifdef DEBUG
	printf("#Loaded %d fasta\n", info->n_fasta);
	print_fasta(info);
#endif				/* DEBUG */

	int i, j = 0;

	for (i = 0; i < info->n_fasta; i++) {
		printf(">%s\n", (info->fas)[i]->id);

		if (!i || !info->f_background) {
			for (j = 0; j < info->n_matrixes; j++) {
				/* !i because at least we have to get ll foreach matrix at the beginning.
				   !info->f_background because if we have different bg for every 
				   fasta we have to do this everytime and not foreach matrix only. */
				assign_ll((info->matrixes)[j], (info->fas)[i], info);
			
				check_error(info);
			}
		}

		if (info->window != 0) {
			if (info->window != -1) {
				for (j = 0; j < info->n_matrixes; j++) {
					double *d = NULL;
					int how_many = 0;
					//printf("%s\n", (info->matrixes)[j]->name);
					d = matrix_run((info->matrixes)[j], (info->fas)[i], &how_many, info);
					check_error(info);
					if (d)
						print_window_affinities(d, info->window - info->matrixes[j]->length + 1, how_many, info->slide, info->matrixes[j]->name);
					free(d);
				}
			} else {
				// info->window == -1 (single window mode)
				for (j = 0; j < info->n_matrixes; j++) {
					if ((!i || !info->f_background) && (info->miRNA | info->occupancy)) { 
						//we need a cutoff for miRNA mode and occupancy
						info->assign_cutoff_pointer((info->matrixes)[j], info);
					}
					int tot_match_noN = 0;
					double tot_aff = matrix_little_window_tot((info->matrixes)[j], (info->fas)[i], 0, -1, &tot_match_noN, info);
					if(info->normalize_on_seq_len){
						tot_aff/=tot_match_noN;
					}
					printf("%s\t%g\n", (info->matrixes)[j]->name, tot_aff);
				}
			}
		} else {
			//single site mode
			for (j = 0; j < info->n_matrixes; j++) {
				if (!i || !info->f_background) {
					info->assign_cutoff_pointer((info->matrixes)[j], info);
				}
				if (info->miRNA)
					find_single_strand_matches((info->matrixes)[j],
		    								   (info->fas[i]), info);
				else
					matrix_cutoff_print((info->matrixes)[j],
						    			(info->fas[i]), info);
				check_error(info);
			}
		}
	}
#ifdef DEBUG
	printf("#Loaded %d matrixes\n", info->n_matrixes);
	print_matrixes(info);
#endif				/* DEBUG */

	tidy(info);
	return OK;
}

/* input.c
//
// Copyright (C) 2009 Elena Grassi <grassi.e@gmail.com>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include "total_affinity.h"

/* 
    Function: parse_command_line
    
    Parses the argv array putting user's choices in the given run.
    Sets info->error and prints a short help message (usage) if
    there is an error in the command line. Opens fasta and matrix
    file and sets the handles in the given run.
    
    Parameters:
    
        argv - char**, same as in main.
        argc - int, same as in main. Number of argv element that will be considered.
        run  - info, used to set error flags and put opened filehandles.
*/
/*extern int getopt(int argc, char * const argv[], const char *optstring); */

void parse_command_line(char *argv[], int argc, run info)
{
	/* extern char *optarg;
	   extern int optind, opterr, optopt; */

	char next_token;

	int set_h = 0;
	int set_f = 0;
	int set_m = 0;
	int set_o = 0;
	int set_w = 0;
	int set_a = 0;
	int set_p = 0;
	int set_b = 0;
	int set_j = 0;
	int index;
	opterr = 0;

	while ((next_token = getopt(argc, argv, ":w:ca:p:f:b:o:m:j:slhMNiznSry")) != -1
	       && !set_h) {
		switch (next_token) {
		case 'h':
			info->error = COMMAND_LINE_ERROR;
			set_h = 1;
			break;
		case 's':
			info->revcomp = 0;
			break;
		case 'l':
			info->log2 = 1;
			break;
		case 'i':
			info->miRNA = 1;
			break;
		case 'M':
			info->only_max = 1;
			break;
		case 'N':
			info->skip_N_regions = 1;	
			break;
		case 'n':
			info->normalize_on_seq_len = 0;
			break;
		case 'S':
			info->single_strand = 1;
			break;
		case 'a':
			if (!is_atofable(optarg) || set_a) {
				info->error = COMMAND_LINE_ERROR;
				fprintf(stderr,
					"Wrong (or double) parameter given to -a, I need a double, "
					"use . as comma separator.\n");
				set_h = 1;
			} else {
				info->abs_cutoff = atof(optarg);
				info->given_abs_cutoff = 1;
				set_a = 1;
			}
			break;
		case 'r':
			info->lower_cutoff_wins = 1;
			break;
		case 'c':
			info->counts = 0;
			break;
		case 'z':
			info->keep_zeroes = 1;
			break;
		case 'p':
			if (!is_atofable(optarg) || set_p
			    || (info->cutoff = atof(optarg)) < 0
			    || (info->cutoff = atof(optarg)) > 1) {
				info->error = COMMAND_LINE_ERROR;
				fprintf(stderr,
					"Wrong (or double) parameter given to -p, I need a double, "
					"use . as comma separator.\n");
				set_h = 1;
			} else {
				info->cutoff = atof(optarg);
				set_p = 1;
			}
			break;
		case 'w':
			if (!is_atoable(optarg) ||
			    ((info->window = atoi(optarg)) < -1) || set_w) {
				info->error = COMMAND_LINE_ERROR;
				fprintf(stderr,
					"Wrong (or double) parameter given to -w, I need a integer >= -1\n");
				set_h = 1;
			} else
				set_w = 1;
			break;
		case 'f':
			if (set_f) {
				fprintf(stderr,
					"Double '-f' parameter given\n");
				info->error = COMMAND_LINE_ERROR;
				set_h = 1;
			}
			set_f = optind - 1;
			break;
		case 'b':
			if (set_b) {
				fprintf(stderr,
					"Double '-b' parameter given\n");
				info->error = COMMAND_LINE_ERROR;
				set_b = 1;
			}
			set_b = optind - 1;
			break;
		case 'o':
			if (set_o) {
				fprintf(stderr,
					"Double '-o' parameter given\n");
				info->error = COMMAND_LINE_ERROR;
				set_o = 1;
			}
			set_o = optind - 1;
			break;
		case 'y':
			info->occupancy = 1;
			break;
		case 'm':
			if (set_m) {
				fprintf(stderr,
					"Double '-m' parameter given\n");
				info->error = COMMAND_LINE_ERROR;
				set_h = 1;
			}
			set_m = optind - 1;
			break;
		case 'j':
			if (!is_atoable(optarg) ||
			    ((info->slide = atoi(optarg)) <= 0) || set_j) {
				info->error = COMMAND_LINE_ERROR;
				fprintf(stderr,
					"Wrong (or double) parameter given to -j, I need a integer >= 1\n");
				set_h = 1;
			} else
				set_j = 1;
			break;
		case '?':
			info->error = COMMAND_LINE_ERROR;
			if (isprint(optopt))
				fprintf(stderr, "Unknown option `-%c'.\n",
					optopt);
			else
				fprintf(stderr,
					"Unknown option character `\\x%x'.\n",
					optopt);
			set_h = 1;
			break;
		case ':':
			info->error = COMMAND_LINE_ERROR;
			set_h = 1;
			fprintf(stderr, "'-%c' needs ad argument!\n", optopt);
			break;
		}
	}
	for (index = optind; index < argc && !set_h; index++)
		fprintf(stderr, "Unknown argument %s, ignoring it\n",
			argv[index]);

	if (set_w && set_j && info->window <= 0) {
		fprintf(stderr,
			"-w -1 or -w 0 are incompatible with option -j!\n");
		set_h = 1;
	}

	if (!(set_w && info->window == 0) && info->only_max) {
		fprintf(stderr,
			"-M is usable only with -w 0 (ss emulation mode)!\n");
		set_h = 1;
	}
	
	if (set_p && set_a && !info->lower_cutoff_wins) {
		fprintf(stderr,
			"-p and -a are incompatible with each other!\n");
		set_h = 1;
	}

	if ((set_p || set_a) && set_o) {
		fprintf(stderr,
			"-p or -a are incompatible with option -o!\n");
		set_h = 1;
	}
	
	if (info->lower_cutoff_wins && !set_a) {
				fprintf(stderr,
			"-r option could be used only with -a\n");
		set_h = 1;
	}
	
	if (((info->window == -1 && !info->occupancy) || info->window > 0) && (set_p || set_a || set_o)) {
		fprintf(stderr,
			"cutoff options (-a, -p, -o) are unavailable with -w -1 without -y or in window mode\n");
		set_h = 1;	
	}
	
	if (info->single_strand && (set_w && info->window == 0)) {
		fprintf(stderr,
			"-S is unavailable when in syteseeker mode (-w 0)\n");
		set_h = 1;
	}
	// TODO: should be available 
	
	if (info->occupancy && (info->window > 0 || set_a || set_o)) {
		fprintf(stderr,
			"-y is available only with -w 0 or -1 and without -a/-o\n");
		set_h = 1;	
	}
	
	if (info->log2 == 1 && !info->occupancy) {
		fprintf(stderr,
			"-l is available only with -y\n");
		set_h = 1;	
	}
	
	if (info->keep_zeroes && (info->log2 || (set_w && info->window == 0))) {
		fprintf(stderr,
			"-z is incompatible with option -l or -w 0!\n");
		set_h = 1;
	}

	if (set_h || !set_m || !set_f) {
		info->error = COMMAND_LINE_ERROR;
		if (!set_m && !set_h)
			fprintf(stderr, "Missing matrix file! -m\n");
		if (!set_f && !set_h)
			fprintf(stderr, "Missing fasta file! -f\n");
		fprintf(stderr,
			"Usage: %s -m matrix_file -f fasta_file window_size\n"
			"[-w window size (0 to emulate ss, -1 to use a window length equal to single matrix, default is 100)\n"
			"[-s to always print straight match, not revcomp (revcomp is default)]\n"
			"[-i used to find absolute counts of perfect matches only on the given strand (seeking miRNAs' targets)]\n"
			"[-S use only the given strand to compute affinities\n"
			"[-c if matrix file has fractions (default is counts)!]\n"
			"[-z if zero counts should not be changed to 1, usable only in not syteseeker mode and without -l]\n"
			"[-a 0 absolute cutoff (not used if not given)]\n"
			"[-p 0 fractional cutoff (default 0.8, use dots!)]\n"
			"[-r 0 relative cutoff: the smaller one between fractional (fraction of the maximum score) and absolute is chosen for each matrix]\n"
			"[-l to use log2likelihoods even if not in syteseeker mode]\n"
			"[-b file with fixed background frequencies that will be used]\n"
			"[-M to be used with -w 0 to print only the biggest match for every fasta]\n"
			"[-o file with cutoffs for every given matrix]\n"
			"[-y perform occupancy analysis, using the fractional cutoff]\n"
			"[-j number of bases \"jumped\" by the sliding window, can be used if -w is != 1 and 0, default is 1]\n"
			"[-N prevent nan printing when the sequence window considered is full of N]\n"
			"[-n do not normalize the total affinity on the sequence length (default: normalize)]\n"
			"\n"
			".META: stdout in -w 0 mode (fasta format, sequence id in the header)\n"
			"	matrix_id	stat3\n"
			"	sequence	TTCCAAGAA\n"
			"	score		10.4799\n"
			"	pos_in_fasta	327\n" "\n", argv[0]);
		return;
	}

	if (open_file(argv, set_f, &(info->f_fasta)) ||
	    open_file(argv, set_m, &(info->f_matrixes)))
		info->error = COMMAND_LINE_ERROR;

	if (set_b)
		if (open_file(argv, set_b, &(info->f_background)))
			info->error = COMMAND_LINE_ERROR;

	if (set_o)
		if (open_file(argv, set_o, &(info->f_cutoffs)))
			info->error = COMMAND_LINE_ERROR;
			
    if (info->miRNA)
    	info->get_affinities_pointer = get_affinities_single_strand;
    	
    if (info->occupancy) {
	    if (info->log2 == 0) {
    		info->assign_cutoff_pointer = assign_cutoff_occupancy;
    		info->get_affinities_ss_pointer = get_affinities_nonLog_ss;
	    } else {
			info->get_affinities_occupancy_pointer = get_affinities_log;    
	    }
    }
}

/* 
    Function: open_file
    
    Opens in read mode a file identified by a string in the given
    position of the given char* array. Prints an error message
    if something goes wrong and returns ERROR (1).
    
    Parameters:
        argv - char** with the filename.
        position - int with the filename position inside the given array.
        to_open - FILE** where the opened file pointer will be put.
        
    Return:
        ERROR (1) if an error occurred, OK (0) otherwise.
*/
int open_file(char *argv[], int position, FILE ** to_open)
{
	*to_open = fopen(argv[position], "r");
	if (!*to_open) {
		fprintf(stderr, "Error opening %s file\n", argv[position]);
		return ERROR;
	}
	return OK;
}

/* 
    Function: is_atoable
    
    Very ugly function to test if the given string is atoable.
    
    Parameters:
        s - char* to be tested for atoability.
        
    Return:
        0 if s is not atoable, 1 otherwise.
*/
int is_atoable(char *s)
{
	int count = 0;
	while (*s != '\0') {		if (!(isdigit(*s) || (*s) == '+' || (*s) == '-'))
			return 0;
		if ((*s) == '+' || (*s) == '-') {
			if (count == 1)
				return 0;
			count++;
		}
		s++;
	}	return 1;
}

/* 
    Function: is_atofable
    
    Very ugly function to test if the given string is atofable.
    Warning: works only with english locales, "," aren't 
    allowed.
    
    Parameters:
        s - char* to be tested for atofability.
        
    Return:
        0 if s is not atoable, 1 otherwise.

*/
int is_atofable(char *s)
{
	int count_dot = 0;
	int count_sign = 0;
	while (*s != '\0') {
		if (!(isdigit(*s) || (*s) == '.' || (*s) == '+' || (*s) == '-'))
			return 0;
		if ((*s) == '.') {
			if (count_dot == 1)
				return 0;
			count_dot++;
		}
		if ((*s) == '+' || (*s) == '-') {
			if (count_sign == 1)
				return 0;
			count_sign++;
		}
		s++;}
	return 1;
}

/* 
    Function: check_error
    
    Checks the error flag in the given run and prints out what has gone wrong.
    Stops the execution for unrecoverable errors, goes on after having warned
    the user otherwise.
    
    Parameters:
        info - run where the error flag has been set by someone.

*/
void check_error(run info)
{
	switch (info->error) {
	case COMMAND_LINE_ERROR:
		break;
	case BACKGROUND_FREQ_ERROR:
		fprintf(stderr,
			"One of the given fasta seq has something wicked,"
			" eg a nucleotide with zero frequency?\n");
		break;
	case MEMORY_ERROR:
		fprintf(stderr, "Bad luck with memory while storing data!\n");
		break;
	case MATRIX_COUNT_ERROR:
		fprintf(stderr,
			"Something wrong with your matrix file, maybe you used -c"
			"\nand you have some zero counts or fractions?\n");
		break;
	case ILLEGAL_FASTA_CHAR_ERROR:
		fprintf(stderr,
			"There is an illegal char in the given fasta file "
			"(or even it is not a fasta).\n");
		break;
	case MATRIX_LIMIT_ERROR:
		fprintf(stderr,
			"The given matrix file has too many matrixes or one of them\n"
			"is too long. Right now I can handle max %d matrixes with\n"
			"max %d position. Either change #defines in total_affinity.h\n"
			"and recompile or complain with my dad\n For now I will exit.\n",
			 MAX_MATRIXES, MAX_MATRIX_LENGTH);
		//info->error = 0;
		break;
	case SHORT_FASTA_WARNING:
		fprintf(stderr,
			"Warning: one of the given fasta was too short to find\n"
			"a match for one of the given matrixes\n");
		info->error = 0;
		break;
	case MATRIX_FILE_ERROR:
		fprintf(stderr,
			"Something wrong with the matrix file. Is it name tab pos tab "
			"a_count tab c_count tab g_count tab t_count?\n");
		break;
	case BG_FILE_ERROR:
		fprintf(stderr,
			"Something wrong with the matrix file. Is it a single tab "
			"delimited line with four background values (> 0)?\n");
		break;
	case CUTOFF_FILE_ERROR:
		fprintf(stderr,
			"Something wrong with the cutoff file. Format error or previous "
			"warnings about missing/double matrixes or invalid fractional cutoff\n");
		break;
	case FREQ_ZERO_ERROR:
		fprintf(stderr,
			"A frequency = 0 was found and -z was not set (< %g)\n", EEEPSILON);
		break;
	}
	if (info->error) {
		fprintf(stderr, "(Not gracefully) exiting...\n");
		tidy(info);
		exit(ERROR);
	}
}

/* 
    Function: tidy
    
    Closes the opened filehandle and frees pointers to matrixes and fastas.
    
    Parameters:
        info - run with filehandles to be closed and pointers to be freed.
*/
void tidy(run info)
{
	if (info->f_fasta)
		fclose(info->f_fasta);
	if (info->f_matrixes)
		fclose(info->f_matrixes);
	if (info->f_background)
		fclose(info->f_background);
	if (info->matrixes)
		free_matrixes(info->matrixes, MAX_MATRIXES);
	if (info->fas)
		free_fasta(info);
	if (info->f_log)
		fclose(info->f_log);
	if (info->f_cutoffs)
		fclose(info->f_cutoffs);
	if (info)
		free(info);
}

/* 
    Function: set_beginning_info
    
    Sets the beginning parameter in the given configuration struct pointer (info).
    
    Parameters:
        info - where we store all default configuration parameters.
*/
int set_beginning_info(run * info)
{
	*info = (run) calloc(1, sizeof(struct run_));
	if (!(*info))
		return ERROR;
	(*info)->window = DEFAULT_WINDOW_SIZE;
	(*info)->revcomp = DEFAULT_REVCOMP;
	(*info)->counts = DEFAULT_COUNT;
	(*info)->cutoff = DEFAULT_CUTOFF;
	(*info)->abs_cutoff = DEFAULT_ABS_CUTOFF;
	(*info)->log2 = DEFAULT_LOG2;
	(*info)->keep_zeroes = DEFAULT_KEEP_ZEROES;
	(*info)->slide = DEFAULT_SLIDE;
	(*info)->occupancy = DEFAULT_OCCUPANCY;
	(*info)->single_strand = 0;
	(*info)->given_abs_cutoff = 0;
	(*info)->lower_cutoff_wins = DEFAULT_LOWER_CUTOFF_WINS;
	(*info)->miRNA = 0;
	(*info)->error = 0;
	(*info)->n_matrixes = 0;
	(*info)->matrixes = NULL;
	(*info)->n_fasta = 0;
	(*info)->fas = NULL;
	(*info)->f_log = NULL;
	(*info)->f_background = NULL;
	(*info)->only_max = 0;
	(*info)->skip_N_regions = 0;
	(*info)->f_cutoffs = NULL;
	(*info)->get_affinities_pointer = get_affinities_nonLog;
	(*info)->get_affinities_occupancy_pointer = get_affinities_nonLog;
	(*info)->assign_cutoff_pointer = assign_cutoff;
	(*info)->get_affinities_ss_pointer = get_affinities;
	(*info)->normalize_on_seq_len = DEFAULT_NORMALIZZE_ON_SEQ_LEN;
	return OK;
}

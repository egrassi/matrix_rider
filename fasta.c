/* fasta.c
// Copyright (C) 2008 Elena Grassi <grassi.e@gmail.com>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but 
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "total_affinity.h"
#include "CuTest.h"

/*
    Function: load_fasta
    
    Loads sequences from a multifasta file and returns an array of fasta structs.
    Warning: it doesn't check in any way that in info we have a proper fasta file,
    		 it just complains in some cases!
    
    Parameters:
    
        info - run with infos, will use the fasta file opened there.
               Error flags will be put there.
        loaded - int * where the number of loaded fasta seq will be put.
  
    Returns:
    
        array of fasta structs loaded from the f_fasta in the given run.
*/
void load_fasta(run info)
{
	double bg[BASES];
	if (info->f_background) {
		get_background_from_file(info, bg);
		check_error(info);
	}

	info->fas = (fasta *) calloc(STARTING_FASTA, sizeof(fasta));
	if (!(info->fas)) {
		printf("#DEBUC %d calloc %d %lu\n", __LINE__, STARTING_FASTA,
		       sizeof(fasta));
		info->error = MEMORY_ERROR;
		return;
	}
	fasta *f = info->fas;
	char c = 'c';
	int char_counter = 0;
	int tot = STARTING_FASTA;
	int length_s = STARTING_SEQ;
	int status = 0;
	/* 0 expected new header or white line, 1 expected new fasta line or white lines or header, 2 reading seq */
	FILE * f_fasta = info->f_fasta;
	while ((c = getc(f_fasta)) != EOF) {
		if (c == '\n') {
			/* new line di fine riga fasta */
			if (status == 2)
				status = 1;
			/* sono al secondo newline, il fasta e` finito obbligatoriamente */
			else if (status == 1
				 || (status == 0 && info->n_fasta != 0)) {
				status = 0;
			}
			/* righe bianche a inizio file: errore */
			else {
				info->error = ILLEGAL_FASTA_CHAR_ERROR;
				return;
			}
		} else if (c == '>' && (status == 0 || status == 1)) {
			status = 1;
			/* store old fasta in actual *f (char_counter to length) */
			if (info->n_fasta) {	/* after first fasta, old data */
				(*f)->length = char_counter;
				if (info->f_background)
					assign_bg_from_file(*f, bg);
				else
					assign_bg(*f, info);
				if (char_counter >= length_s) {
					((*f)->seq) =
					    (char *)realloc((*f)->seq,
							    (length_s +
							     1) * sizeof(char));
					if (!(*f)->seq) {
						printf
						    ("#DEBUC %d realloc %d %lu\n",
						     __LINE__, length_s + 1,
						     sizeof(char));
						info->error = MEMORY_ERROR;
						return;
					}
				}
				((*f)->seq)[char_counter] = '\0';
				if (info->n_fasta >= tot) {
					info->fas =
					    (fasta *) realloc(info->fas,
							      (tot +
							       STARTING_FASTA) *
							      sizeof(fasta));
					if (!info->fas) {
						printf
						    ("#DEBUC %d realloc %d %lu\n",
						     __LINE__,
						     tot + STARTING_FASTA,
						     sizeof(fasta));
						info->error = MEMORY_ERROR;
						return;
					}
					tot += STARTING_FASTA;
				}
				if (info->error == BACKGROUND_FREQ_ERROR) {
					/* we skip fasta with all N chars */
					fprintf(stderr,
						"Error with %s fasta, all 'N' or even no seq at all?\n",
						(*f)->id);
					free((*f)->seq);
					free((*f)->id);
					free(*f);
					*f = NULL;
					(info->n_fasta)--;
					info->error = 0;
				}
				f = info->fas + info->n_fasta;
			}
			*f = (fasta) calloc(1, sizeof(struct fas));
			if (!*f) {
				printf("#DEBUC %d calloc %d %lu\n", __LINE__, 1,
				       sizeof(struct fas));
				info->error = MEMORY_ERROR;
				return;
			}
			assign_zero_bg(*f);
			(*f)->seq = (char *)calloc(STARTING_SEQ, sizeof(char));
			if (!(*f)->seq) {
				printf("#DEBUC %d calloc %d %lu\n", __LINE__,
				       STARTING_SEQ, sizeof(char));
				info->error = MEMORY_ERROR;
				free(*f);
				return;
			}
			length_s = STARTING_SEQ;
			char_counter = 0;
			get_id(info->f_fasta, &((*f)->id), info);
			(info->n_fasta)++;
		}
		/* se ho un carattere e sono a inizio fasta o in mezzo ad una riga va bene */
		else if (c != '\n' && (status == 1 || status == 2)) {
			status = 2;	/* siamo in una riga del fasta */
			if (char_counter >= length_s) {
				(*f)->seq =
				    (char *)realloc((*f)->seq,
						    (length_s +
						     STARTING_SEQ) *
						    sizeof(char));
				if (!(*f)->seq) {
					printf("#DEBUC %d realloc %d %lu\n",
					       __LINE__,
					       length_s + STARTING_SEQ,
					       sizeof(char));
					info->error = MEMORY_ERROR;
					return;
				}
				length_s += STARTING_SEQ;
			}
			((*f)->seq)[char_counter] = toupper(c);
			add_count_bg(*f, ((*f)->seq)[char_counter], info);
			check_error(info);
			char_counter++;
		}
		/* righe bianche in mezzo ad un fasta o fasta header non a capo: errore */
		else {
			info->error = ILLEGAL_FASTA_CHAR_ERROR;
			return;
		}
	}
	if (info->n_fasta) {
		(*f)->length = char_counter;	/* store last fasta informations */
		if (info->f_background)
			assign_bg_from_file(*f, bg);
		else
			assign_bg(*f, info);
		if (char_counter >= length_s) {
			(*f)->seq =
			    (char *)realloc((*f)->seq,
					    (length_s + 1) * sizeof(char));
			if (!(*f)->seq) {
				printf("#DEBUC %d calloc %d %lu\n", __LINE__,
				       length_s + 1, sizeof(char));
				info->error = MEMORY_ERROR;
				return;
			}
		}
		((*f)->seq)[char_counter] = '\0';
		if (info->error == BACKGROUND_FREQ_ERROR) {
			fprintf(stderr,
				"Error with %s fasta, all 'N' or even no seq at all?\n",
				(*f)->id);
			free((*f)->seq);
			free((*f)->id);
			free(*f);
			*f = NULL;
			f--;
			(info->n_fasta)--;
			info->error = 0;
			info->fas =
			    (fasta *) realloc(info->fas,
					      (info->n_fasta) * sizeof(fasta));
		}
#ifdef DEBUG
		if (info->n_fasta)
			printf("%d Loaded %s\n", char_counter, (*f)->seq);
#endif				/* DEBUG */
	}
}

/*
    Function: assign_zero_bg
    
    Sets all background frequencies in the given fasta to 0.
    
    Parameters:
        f - fasta with background to be prepared to add background data.
*/
void assign_zero_bg(fasta f)
{
	int i = 0;
	for (; i < BASES; i++)
		f->background[i] = 0;
	f->n_portions = 0;
}

/*
   Function: add_count_bg
   
   Adds the given char to the background count of the given fasta.
   If a char outside the allowed scope (/ACTGN/i) is given an error
   flag will be put in info.
   
   Parameters:
        f - fasta with background info being charged.
        c - char to be counted.
        info - run to issue the eventual error flag.
*/
void add_count_bg(fasta f, char c, run info)
{
	switch (c) {
	case ('A'):
		f->background[A]++;
		break;
	case ('C'):
		f->background[C]++;
		break;
	case ('G'):
		f->background[G]++;
		break;
	case ('T'):
		f->background[T]++;
		break;
	case ('N'):
		break;
	default:
		info->error = ILLEGAL_FASTA_CHAR_ERROR;
	}
}

/* 
    Function: assign_bg
    
    Given a fasta evaluates the correct background frequencies, using stored 
    count for ACGT and length, correcting the fraction without considering
    the 'N' chars in the total length (WARNING: characters outside the /ACGTN/i
    scope are not considered and will cause unpredictable results).
    Note that a frequency of zero for a nucleotide
    is not allowed and will cause an error flag to be issued in the info parameter.
    
    Parameters:
        f - fasta with ACGTN count and length stored.
        info - run where an error flag could be issued if a nucleotide frequency
               will result 0 (to always respect assign_ll p-in).
*/
void assign_bg(fasta f, run info)
{
	int i = 0;
	f->ACGTlength = 0;
	int noBase = 0;
	for (; i < BASES; i++) {
		if (f->background[i] <= EPSILON) {
			f->background[i] = 1;
			noBase++;
			fprintf(stderr,
				"WARNING: the base %d has zero count in fasta block %s\n",
				i, f->id);
		}
		f->ACGTlength += f->background[i];
	}
	if (noBase == 4) {
		info->error = BACKGROUND_FREQ_ERROR;
		return;
	}
	for (i = 0; i < BASES; i++)
		f->background[i] = f->background[i] / f->ACGTlength;
}

//XXX
void get_background_from_file(run info, double *bg)
{
	if (fscanf
	    (info->f_background, "%lf\t%lf\t%lf\t%lf\n", &bg[A], &bg[C], &bg[G],
	     &bg[T]) != 4)
		info->error = BG_FILE_ERROR;
	if (bg[A] < EEEPSILON || bg[C] < EEEPSILON
	    || bg[G] < EEEPSILON || bg[T] < EEEPSILON)
		info->error = BG_FILE_ERROR;
}

//XXX
void assign_bg_from_file(fasta f, double *bg)
{
	int i = 0;
	f->ACGTlength = 0;
	for (; i < BASES; i++) {
		if (f->background[i] <= EPSILON) {
			f->background[i] = 1;
			fprintf(stderr,
				"WARNING: the base %d as zero count in fasta block %s\n",
				i, f->id);
		}
		f->ACGTlength += f->background[i];
		f->background[i] = bg[i];
	}
}

// XXX unused
/*
    Function: skip_line
    
    Eats all chars in f (read opened) until it finds a newline or EOF.
    
    Parameters:
    
        f - fasta * with the line to be consumed.
*/
void skip_line(FILE * f)
{
	char c;
	while ((c = fgetc(f)) != '\n' && c != EOF) ;
}

/*
    Function: free_fasta
    
    Frees the space used by all the loaded fastas.
    
    Parameters:
    
        f - fasta * with the fastas to be freed.
*/
void free_fasta(run info)
{
	int i = 0;
	for (i = 0; i < info->n_fasta; i++) {
		free((info->fas)[i]->id);
		free((info->fas)[i]->seq);
		free((info->fas)[i]);
	}
	free(info->fas);

}

/*
    Function: get_id
    
    Puts all chars in f in s (read opened), until it finds a newline or EOF.
    S should have enough space to store MAX_FASTA_ID characters (including \0).
    
    Parameters:
    
        f - fasta * with the line to be consumed.
        s - char * where the line contents will be stored.
*/
void get_id(FILE * f, char **s, run info)
{
	char c;
	*s = (char *)calloc(MAX_FASTA_ID, sizeof(char));
	if (!*s) {
		info->error = MEMORY_ERROR;
		check_error(info);
	}
	int i = 0;
	int length = MAX_FASTA_ID;
	while ((c = fgetc(f)) != '\n') {
		if (i == length) {
			*s = (char *)realloc(*s, length * 2 * sizeof(char));
			if (!*s) {
				info->error = MEMORY_ERROR;
				check_error(info);
			}
			length *= 2;
		}
		(*s)[i] = c;
		i++;
	}
	if (i == length) {
		*s = (char *)realloc(*s, (length + 1) * sizeof(char));
		if (!*s) {
			info->error = MEMORY_ERROR;
			check_error(info);
		}
		length *= 2;
	}
	(*s)[i] = '\0';
}

/*
    Function: get_seq
    
    Extracts a substring from seq, starting at offset and getting len chars,
    the resulting string will be put in match (<b>caution</b>: caller must 
    ensure that match is long enough).
    
    Parameters:
        seq - char * from which the substring is extracted.
        offset - int that points at the beginning of the desided substring.
        len - int, how many chars will be gotten.
        match - char * where the resulting substring will be put.
    
*/
void get_seq(char *seq, int offset, int len, char *match)
{
	int i, j;
	for (j = 0, i = offset; j < len; j++, i++)
		*(match + j) = seq[i];
	*(match + j) = '\0';
}

/*
    Function: get_rc
    
    Extracts the revcomp of a substring of seq, starting at offset 
    and getting len chars, the resulting string will be put in match
    (<b>caution</b>: caller must ensure that enough match is long enough).
    
    Parameters:
        seq - char * from which the revcomp is extracted.
        offset - int that points at the end of the desided revcomp.
        len - int, how many chars will be revcomped.
        match - char * where the resulting revcomp will be put.
    
*/
void get_rc(char *seq, int offset, int len, char *match)
{
	int i, j;
	for (j = 0, i = offset + len - 1; i >= offset; j++, i--) {
		switch (seq[i]) {
		case 'A':
			*(match + j) = 'T';
			break;
		case 'C':
			*(match + j) = 'G';
			break;
		case 'G':
			*(match + j) = 'C';
			break;
		case 'T':
			*(match + j) = 'A';
			break;
		default:
			*(match + j) = 'N';
		}
	}
	*(match + j) = '\0';
}
